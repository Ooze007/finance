<?php
// src/AppBundle/Entity/Quote.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use GuzzleHttp\Client as GuzzleClient;
use AppBundle\Model\QuoteInfo;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_quote")
 */
class Quote
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Symbol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="symbol_id", referencedColumnName="id")
     * })
     */
    private $symbol;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $count;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="quotes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function __construct()
    {
        // your own logic
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set symbol
     *
     * @param \AppBundle\Entity\Symbol $symbol
     * @return Quote
     */
    public function setSymbol(\AppBundle\Entity\Symbol $symbol = null)
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * Get symbol
     *
     * @return \AppBundle\Entity\Symbol 
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Set count
     *
     * @param integer $count
     * @return Quote
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer 
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Quote
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return \AppBundle\Model\QuoteInfo
     */
    public function getInfo()
    {
        $client = new GuzzleClient();
        $res = $client->request('GET', 'https://query.yahooapis.com/v1/public/yql', [
            'query' => [
                'q' => sprintf(
                    'select * from yahoo.finance.quotes where symbol="%s"',
                    $this->symbol->getSymbol()
                ),
                'format' => 'json',
                'env' => 'store://datatables.org/alltableswithkeys'
            ]
        ]);
        
        $result = json_decode($res->getBody());

        return new QuoteInfo(
            $result->query->results->quote->Bid,
            $result->query->results->quote->Ask
        );
    }
}
