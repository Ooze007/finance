<?php
// src/AppBundle/Entity/SymbolHistoriy.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_historiy", uniqueConstraints={@UniqueConstraint(name="history_idx", columns={"symbol_id", "date"})})
 */
class SymbolHistoriy
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Symbol")
     * @ORM\JoinColumn(name="symbol_id", referencedColumnName="id")
     */
    private $symbol;

    /**
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @ORM\Column(name="price", type="decimal", nullable=false)
     */
    private $price;

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return SymbolHistoriy
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set symbol
     *
     * @param \AppBundle\Entity\Symbol $symbol
     * @return SymbolHistoriy
     */
    public function setSymbol(\AppBundle\Entity\Symbol $symbol = null)
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * Get symbol
     *
     * @return \AppBundle\Entity\Symbol 
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return SymbolHistoriy
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }
}
