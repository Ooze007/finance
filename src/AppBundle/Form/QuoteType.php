<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuoteType extends AbstractType
{
    /**
     * Quote form create
     * @param  FormBuilderInterface $builder
     * @param  array                $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('symbol', null, array('label' => 'form.symbol', 'translation_domain' => 'AppBundle'))
            ->add('count', null, array('label' => 'form.count', 'translation_domain' => 'AppBundle'));
    }
    
    /**
     * Quote form set options
     * @param  OptionsResolver $resolver
     * @return viod
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Quote'
        ));
    }

    /**
     * Get prefix
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'appbundle_quote';
    }
}
