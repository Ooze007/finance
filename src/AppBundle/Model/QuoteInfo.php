<?php

namespace AppBundle\Model;

/**
* Quote external info
*/
class QuoteInfo
{
    private $bid = 0;
    private $ask = 0;

    /**
     * @param float $bid
     * @param float $ask
     */
    function __construct($bid = 0, $ask = 0)
    {
        $this->setBid($bid);
        $this->setAsk($ask);
    }

    /**
     * Set bid
     *
     * @param float $bid
     * @return \AppBundle\Model\QuoteInfo
     */
    public function setBid($bid)
    {
        $this->bid = $bid;
        
        return $this;
    }

    /**
     * Get bid
     *
     * @return float
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set ask
     *
     * @param float $ask
     * @return \AppBundle\Model\QuoteInfo
     */
    public function setAsk($ask)
    {
        $this->ask = $ask;
        
        return $this;
    }

    /**
     * Get ask
     *
     * @return float
     */
    public function getAsk()
    {
        return $this->ask;
    }
}
