<?php
// src/AppBundle/Command/LoadQuoteCommand.php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client as GuzzleClient;
use AppBundle\Entity\SymbolHistoriy;

class LoadQuoteCommand extends ContainerAwareCommand
{
    protected function configure()
    {
    $this
        ->setName('app:load-quotes')
        ->setDescription('Load quotes.')
        ->setHelp("This command load quotes");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Run!');
        $em = $this->getContainer()->get('doctrine')->getManager();
    
        $symbols = $em->getRepository('AppBundle:Symbol')->findAll();
        
        $client = new GuzzleClient();
        $now  = new \DateTime();
        
        foreach($symbols as $symbol)
        {
            $endDate = clone $now;

            $output->writeln($symbol->getSymbol());

            for($i=0; $i < 24; $i++)
            {
                $startDate = clone $endDate;
                $startDate->sub(new \DateInterval('P1M'));

                $res = $client->request('GET', 'https://query.yahooapis.com/v1/public/yql', [
                    'query' => [
                        'q' => sprintf(
                            'select * from yahoo.finance.historicaldata where symbol="%s" and startDate="%s" and endDate="%s"',
                            $symbol->getSymbol(),
                            $startDate->format('Y-m-d'),
                            $endDate->format('Y-m-d')
                        ),
                        'format' => 'json',
                        'env' => 'store://datatables.org/alltableswithkeys'
                    ]
                ]);

                $result = json_decode($res->getBody());
                
                foreach($result->query->results->quote as $quote)
                {
                    $output->writeln($quote->Date);
                    $output->writeln($quote->Close);
                    $date = \DateTime::createFromFormat('Y-m-d H:i:s', $quote->Date.' 00:00:00');

                    $historyItem = new SymbolHistoriy();
                    $historyItem
                        ->setSymbol($symbol)
                        ->setDate($date)
                        ->setPrice((float)$quote->Close);

                    $em->persist($historyItem);
                    $em->flush();
                }

                $endDate = $startDate;
                $startDate->sub(new \DateInterval('P1D'));
            }

            $output->writeln("");
        }
    }
}
